import assert  from 'assert';
import Promise from 'bluebird';
import _       from 'lodash';

import memorized, { memorize } from '../index';

function measureTimeSpent(fn, ...args) {
    let startTime = new Date();
    let result = fn(...args);
    if (result && _.isFunction(result.then)) {
        return result.then(() => {
            let timeSpent = new Date() - startTime;
            return timeSpent;
        });
    }
    let timeSpent = new Date() - startTime;
    return timeSpent;
}

function heavyLoad(n) {
    let result = 0;
    for (let i = 1; i <= n; i++) {
        result += i;
    }
    return result;
}

function delay(result, t) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(_.isFunction(result) ? result() : result);
        }, t);
    });
}

async function asyncFunction(v) {
    return await delay(v, 300);
}

const syncStore = function() {
    return {};
};
const asyncStore = function() {
    return {
        get : async function(key) {
            return this[key];
        },
        set : async function(key, value) {
            this[key] = value;
        }
    }
};

class TestClassWithSyncStore {
    constructor(v) {
        this.v = v;
    }

    @memorized({ store : syncStore() })
    getValueSync(n) {
        return heavyLoad(n)*this.v;
    }

    @memorized({ store : syncStore() })
    getValueAsync = async n => {
        return await asyncFunction(n*this.v);
    }
}

class TestClassWithAsyncStore {
    constructor(v) {
        this.v = v;
    }

    @memorized({ store : asyncStore(), storeSync : false })
    getValueSync(n) {
        return heavyLoad(n)*this.v;
    }

    @memorized({ store : asyncStore(), storeSync : false })
    getValueAsync = async n => {
        return await asyncFunction(n*this.v);
    }
}

const testValue = 10000000;

describe('memorize decorator', function() {

    // Test function decorator
    it('decorate function with sync store', function() {
        let fn = memorize({ store : syncStore() })(heavyLoad);
        let timeSpentWithoutCache = measureTimeSpent(fn, testValue);
        let timeSpentWithCache = measureTimeSpent(fn, testValue);
        assert.equal(timeSpentWithoutCache > timeSpentWithCache, true);
    });

    it('decorate async function with sync store', async function() {
        let fn = memorize({ store : syncStore() })(asyncFunction);
        let timeSpentWithoutCache = await measureTimeSpent(fn, testValue);
        let timeSpentWithCache = await measureTimeSpent(fn, testValue);
        assert.equal(timeSpentWithoutCache > timeSpentWithCache, true);
    });

    it('decorate function with async store', async function() {
        let fn = memorize({ store : asyncStore(), storeSync : false })(heavyLoad);
        let timeSpentWithoutCache = await measureTimeSpent(fn, testValue);
        let timeSpentWithCache = await measureTimeSpent(fn, testValue);
        assert.equal(timeSpentWithoutCache > timeSpentWithCache, true);
    });

    it('decorate async function with async store', async function() {
        let fn = memorize({ store : asyncStore(), storeSync : false })(asyncFunction);
        let timeSpentWithoutCache = await measureTimeSpent(fn, testValue);
        let timeSpentWithCache = await measureTimeSpent(fn, testValue);
        assert.equal(timeSpentWithoutCache > timeSpentWithCache, true);
    });

    // Test method decorator
    it('decorate method of object with sync store', function() {
        let obj = new TestClassWithSyncStore(10);
        let fn = ::obj.getValueSync;
        let timeSpentWithoutCache = measureTimeSpent(fn, testValue);
        let timeSpentWithCache = measureTimeSpent(fn, testValue);
        assert.equal(timeSpentWithoutCache > timeSpentWithCache, true);
    });

    it('decorate async method of object with sync store', async function() {
        let obj = new TestClassWithSyncStore(10);
        let fn = ::obj.getValueAsync;
        let timeSpentWithoutCache = await measureTimeSpent(fn, testValue);
        let timeSpentWithCache = await measureTimeSpent(fn, testValue);
        assert.equal(timeSpentWithoutCache > timeSpentWithCache, true);
    });

    it('decorate method of object with async store', async function() {
        let obj = new TestClassWithAsyncStore(10);
        let fn = ::obj.getValueSync;
        let timeSpentWithoutCache = await measureTimeSpent(fn, testValue);
        let timeSpentWithCache = await measureTimeSpent(fn, testValue);
        assert.equal(timeSpentWithoutCache > timeSpentWithCache, true);
    });

    it('decorate async method of object with async store', async function() {
        let obj = new TestClassWithAsyncStore(10);
        let fn = ::obj.getValueAsync;
        let timeSpentWithoutCache = await measureTimeSpent(fn, testValue);
        let timeSpentWithCache = await measureTimeSpent(fn, testValue);
        assert.equal(timeSpentWithoutCache > timeSpentWithCache, true);
    });

});
