import Promise from 'bluebird';
import _       from 'lodash';
import Debug   from 'debug';

const debug = Debug('memorize');

export default function memorizeDecorator(options = {}) {

	return (ctx, key, descriptor) => {

		let initializer = descriptor.initializer;
		if (initializer) {
			descriptor.initializer = function() {
				return memorize(options)(initializer.call(this));
			}
		} else {
			let fn       = descriptor.value;
			let propType = 'value';

			let origGetter = descriptor.get;
			if (origGetter) {
				fn = origGetter;
				propType = 'get';
			}

			if (!_.isFunction(fn))
				throw new TypeError('Invalid property type for memorize decorator');

			descriptor[propType] = memorize(options)(fn);
		}

		return descriptor;
	}
}

export const memorize = function memorize(options = {}) {

	_.defaults(options, {
		store     : {},
		storeSync : true,
		getMethod : 'get',
		setMethod : 'set'
	});

	if (!_.isFunction(options.store[options.getMethod])) {
		options.store[options.getMethod] = function(key) {
			return options.store[key];
		};
	}

	if (!_.isFunction(options.store[options.setMethod])) {
		options.store[options.setMethod] = function(key, value) {
			options.store[key] = value;
		}
	}

	let {
		store,
		storeSync,
		getMethod,
		setMethod
	} = options;

	return function (fn) {

		return function(...args) {
			let key = JSON.stringify(toSortedObject(args));

			if (!_.isUndefined(this)) {
				fn = this::fn;
				debug('context:', this);
			}

			if (storeSync) {
				let cached = store[getMethod](key);

				debug('cached:', cached);

				if (!_.isUndefined(cached))
					return cached;


				let result = fn(...args);

				if (_.isUndefined(result))
					return;

				if (_.isFunction(result.then)) {
					return result.then(resolved => {
						store[setMethod](key, resolved);
						return resolved;
					});
				}
				store[setMethod](key, result);
				return result;
			}

			return store[getMethod](key).then(cached => {
				debug('cached:', cached);
				if (!_.isUndefined(cached))
					return cached;
				return Promise.resolve(fn(...args)).then(result => {
					return store[setMethod](key, result)
						.then(() => result);
				});
			});

		}
	}
}

function toSortedObject(v) {
	if (v instanceof Array) {
		return v.map(toSortedObject);
	}
	if (v instanceof Object) {
		let result = [];
		for (let key in v) {
			result.push({ [key]: toSortedObject(v[key]) });
		}
		return result.sort((a, b) => Object.keys(a)[0] > Object.keys(b)[0])
	}
	return v;
}
